# Suicide data
## Summary
This project contains an analysis of data on sucide rates in various 
countries around the world and over years ranging from 1985 until 2016.

The project foucses mainly on visualisation and identification of patterns
in the data. However, there is not enough data for a more in-depth analysis
with regards to public health and sucicide prevention. 

## Licence
Everything but the raw data (located in the `data-raw` directory), 
is available under CC BY-NC-SA 4.0, which means you can share it as long as you
give credit, do so under the same licence and not use it commercially. 
See file LICENCE.

The data is available under CC BY 4.0 (see below), which means you can re-use
it as long as you credit the original source.

## The data
The data (available in the direcotry `data-raw`) was taken [from Kaggle](https://www.kaggle.com/russellyates88/suicide-rates-overview-1985-to-2016/), 
but the underlying data is actaully from the world bank.
Their [terms of use](https://www.worldbank.org/en/about/legal/terms-of-use-for-datasets) specify that the data is available under 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

## Running
Run the files in `R` in numerical order, afterwards you will be able to 
build the RMarkdown document.

## Dependencies
Running the analysis for yourself requires the following packages:
`broom`, `countrycode`, `dplyr`, `ggplot2`, `here`, `magrittr`, `purrr`,
`readr`, `stringr`, `tibble` and `tidyr`.

For building the RMarkdown document you need `knitr` and `RMarkdown`.

